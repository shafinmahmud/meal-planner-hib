package net.therap.mealplanner.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author shafin
 * @since 11/22/16
 */
public class DateTimeUtil {

    public static Date getDateFromHour(int hour, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DATE, 0);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.YEAR, 0);

        cal.set(Calendar.HOUR, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static String getHourFromDate(Date date) {
        DateFormat format = new SimpleDateFormat("hh.mm aa");
        return format.format(date).replace("AM", "am").replace("PM", "pm");
    }

    public static void main(String[] args) {
        System.out.println(getDateFromHour(21, 10));
        System.out.println(getDateFromHour(21, 10));
    }
}
