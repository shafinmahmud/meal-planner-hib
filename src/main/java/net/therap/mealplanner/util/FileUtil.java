package net.therap.mealplanner.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author shafin
 * @since 11/20/16
 */
public class FileUtil {

    public static Properties getHibernatePropertyValues(String propertyFileName) {

        InputStream inputStream = FileUtil.class.getClassLoader().getResourceAsStream(propertyFileName);

        try {
            Properties prop = new Properties();

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException(propertyFileName + " file not found in the classpath");
            }

            return prop;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getHibernatePropertyValues("hibernate.properties"));
    }
}
