package net.therap.mealplanner.service;

import net.therap.mealplanner.controller.ConsoleView;
import net.therap.mealplanner.dao.ItemDao;
import net.therap.mealplanner.dao.MealDao;
import net.therap.mealplanner.dao.MealTypeDao;
import net.therap.mealplanner.dto.TabularDto;
import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import net.therap.mealplanner.entity.MealType;
import net.therap.mealplanner.util.DateTimeUtil;

import java.util.*;

/**
 * @author shafin
 * @since 11/13/2016
 */
public class MealService {

    private ItemDao itemDao;
    private MealDao mealDao;
    private MealTypeDao mealTypeDao;

    public MealService() {
        this.itemDao = new ItemDao();
        this.mealDao = new MealDao();
        this.mealTypeDao = new MealTypeDao();
    }

    public TabularDto getMenuTable() {
        TabularDto dto = new TabularDto();
        dto.setBanner(ConsoleView.PLANNED_MENUS);
        dto.setTitle(ConsoleView.MENU_TITLE);

        List<String> options = new ArrayList<>();
        options.add("1. To Remove a Plan saveOrUpdate D[id]");
        options.add("2. To Update a Plan saveOrUpdate U[id] [DAY-MEAL_TIME-{item1,item2}]");
        options.add("3. To Add a Plan saveOrUpdate I[DAY-TYPE-HOUR-{item1,item2}]");
        options.add("4. Return Back to Home");
        options.add("0. Exit");
        dto.setOptions(options);

        dto.setCount(mealDao.countAll());
        dto.setTableHead(String.format("%5s%10s%15s%10s%25s", "#id", "day", "type", "hour", "Item"));
        dto.setTableHead(dto.getTableHead() + "\n" + ConsoleView.SEPARATION_LINE);

        List<Meal> meals = mealDao.findAll();
        System.out.println("Number of list item : " + meals.size());
        List<String> tableRows = new ArrayList<>();
        for (Meal meal : meals) {
            Set<Item> items = mealDao.findItemSetOfMeal(meal.getId());
            tableRows.add(String.format("%5s%10s%15s%10s%25s", meal.getId(),
                    meal.getDay(),
                    meal.getMealType().getType(),
                    DateTimeUtil.getHourFromDate(meal.getMealType().getHour()),
                    Item.listToString(items)));
        }
        tableRows.add(ConsoleView.SEPARATION_LINE);
        dto.setTableRows(tableRows);

        return dto;
    }

    public void deleteMenu(int id) {
        mealDao.delete(id);
    }

    public void insertMeal(String dayTxt, String type, int hour, String items) {

        Meal.Day day = Meal.resolveDayType(dayTxt);
        Date hourDate = DateTimeUtil.getDateFromHour(hour, 0);

        MealType mealType = mealTypeDao.findByTypeHour(type, hourDate);

        if (mealType == null) {

            mealType = new MealType();
            mealType.setHour(hourDate);
            mealType.setType(type);
            mealTypeDao.saveOrUpdate(mealType);
        }

        Meal meal = mealDao.findByDayMealType(day, mealType);

        if (meal == null) {
            meal = new Meal();
            meal.setDay(day);
            meal.setMealType(mealType);
        }

        String[] itemArray = items.split(",");
        List<Item> itemList = new ArrayList<>();
        for (String name : itemArray) {

            Item item = itemDao.findByName(name);
            if (item == null) {
                item = new Item(name);
                itemDao.saveOrUpdate(item);
            }

            itemList.add(item);
        }

        meal.setItemList(itemList);
        mealDao.saveOrUpdate(meal);
    }

    public void updateMeal(int id, String dayTxt, String type, int hourInteger, String items) {
        Meal.Day day = Meal.resolveDayType(dayTxt);
        Date hour = DateTimeUtil.getDateFromHour(hourInteger, 0);

        Meal meal = mealDao.find(id);

        if (meal == null) {
            meal = new Meal();
        }

        MealType mealType = mealTypeDao.findByTypeHour(type, hour);

        if (mealType == null) {
            mealType = new MealType();
        }

        mealType.setHour(hour);
        mealType.setType(type);
        mealTypeDao.saveOrUpdate(mealType);

        meal.setDay(day);
        meal.setMealType(mealType);

        String[] itemArray = items.split(",");
        List<Item> itemList = new ArrayList<>();
        for (String name : itemArray) {

            Item item = itemDao.findByName(name);
            if (item == null) {
                item = new Item(name);
                itemDao.saveOrUpdate(item);
            }

            itemList.add(item);
        }

        meal.setItemList(itemList);
        mealDao.saveOrUpdate(meal);
    }

    public static void main(String args[]) {
        MealService mealService = new MealService();
        mealService.insertMeal("Monday", "Lunch", 9, "Rice,Veg.,Egg");
        mealService.updateMeal(1, "Sun", "Breakfast", 10, "Bread,jelly");

        //u[1][Sun-Breakfast-10-{Bread,Jelly}]
    }
}
