package net.therap.mealplanner.service;

import net.therap.mealplanner.controller.ConsoleView;
import net.therap.mealplanner.dao.ItemDao;
import net.therap.mealplanner.dto.TabularDto;
import net.therap.mealplanner.entity.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/14/16
 */
public class ItemService {

    private ItemDao itemDao;

    public ItemService() {
        this.itemDao = new ItemDao();

    }

    public TabularDto getItemsTable() {
        TabularDto dto = new TabularDto();
        dto.setBanner(ConsoleView.AVAILABLE_ITEMS);
        dto.setTitle(ConsoleView.ITEMS_TITLE);

        List<String> options = new ArrayList<>();
        options.add("1. To Remove an Item enter D[id]");
        options.add("2. To Update an Item enter U[id] [new name]");
        options.add("3. To Add an Item enter I[item name]");
        options.add("4. Return Back to Home");
        options.add("0. Exit");
        dto.setOptions(options);

        dto.setCount(itemDao.countAll());
        dto.setTableHead(String.format("%20s%15s", "#id", "item"));
        dto.setTableHead(dto.getTableHead() + "\n" + ConsoleView.SEPARATION_LINE);

        List<Item> itemList = itemDao.findAll();
        List<String> tableRows = new ArrayList<>();
        for (Item item : itemList) {
            tableRows.add(String.format("%20s%15s", item.getId(), item.getName()));
        }
        tableRows.add(ConsoleView.SEPARATION_LINE);
        dto.setTableRows(tableRows);
        return dto;
    }

    public void insertItems(String items) {
        String[] itemArray = items.split(",");
        for (String itemName : itemArray) {
            Item itemEntity = new Item();
            itemEntity.setName(itemName);

            if (itemDao.findByName(itemName) == null) {
                itemDao.saveOrUpdate(itemEntity);
            }
        }
    }

    public void updateItem(int id, String newName) {
        Item item = itemDao.find(id);
        if (item != null) {
            item.setName(newName);
            itemDao.update(item);
        }
    }

    public void deleteItem(int id) {
        if (!itemDao.isAssociatedWithMeal(id)) {
            itemDao.delete(id);
        }else{
            itemDao.deleteItemRelations(id);
            itemDao.delete(id);
        }
    }
}
