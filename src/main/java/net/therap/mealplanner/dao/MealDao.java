package net.therap.mealplanner.dao;

import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import net.therap.mealplanner.entity.MealType;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.Set;

/**
 * @author shafin
 * @since 11/13/16
 */
public class MealDao extends GenericDao<Meal> {

    public Meal findByDayMealType(Meal.Day day, MealType type) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meal.class);
        criteria.add(Restrictions.eq("day", day));
        criteria.add(Restrictions.eq("mealType", type));
        Meal meal = (Meal) criteria.uniqueResult();

        session.getTransaction().commit();
        return meal;
    }

    public Set<Item> findItemSetOfMeal(long id) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meal.class);
        criteria.add(Restrictions.eq("id", id));
        Meal meal = (Meal) criteria.uniqueResult();

        Set<Item> items = new HashSet<>();
        for (Item item : meal.getItemList()) {
            items.add(item);
        }
        session.getTransaction().commit();

        return items;
    }
}
