package net.therap.mealplanner.dao;

import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
public class ItemDao extends GenericDao<Item> {

    public Item findByName(String itemName) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Item.class);
        criteria.add(Restrictions.eq("name", itemName));
        Item item = (Item) criteria.uniqueResult();

        session.getTransaction().commit();
        return item;
    }

    public boolean isAssociatedWithMeal(long id) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meal.class);
        criteria.createAlias("itemList", "i");
        criteria.add(Restrictions.eq("i.id", id));
        Meal meal = (Meal) criteria.uniqueResult();

        session.getTransaction().commit();
        return meal != null;
    }

    public void deleteItemRelations(long id) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meal.class);
        criteria.createAlias("itemList", "i");
        criteria.add(Restrictions.eq("i.id", id));
        List<Meal> mealList = criteria.list();

        for (Meal meal : mealList) {

            List<Item> remainingItems = new ArrayList<>();
            for(Item item : meal.getItemList()){
                if(item.getId() != id){
                    remainingItems.add(item);
                }
            }

            meal.setItemList(remainingItems);
            session.saveOrUpdate(meal);
        }

        session.getTransaction().commit();
    }
}
