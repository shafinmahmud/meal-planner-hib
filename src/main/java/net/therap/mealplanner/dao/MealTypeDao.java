package net.therap.mealplanner.dao;

import net.therap.mealplanner.entity.MealType;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.Date;

/**
 * @author shafin
 * @since 11/13/16
 */
public class MealTypeDao extends GenericDao<MealType> {

    public MealType findByTypeHour(String type, Date hour) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(MealType.class);
        criteria.add(Restrictions.eq("type", type));
        criteria.add(Restrictions.eq("hour", hour));
        MealType mealType = (MealType) criteria.uniqueResult();

        session.getTransaction().commit();
        return mealType;
    }
}
