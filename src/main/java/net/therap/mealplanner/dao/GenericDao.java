package net.therap.mealplanner.dao;

import net.therap.mealplanner.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@SuppressWarnings("unchecked")
public abstract class GenericDao<T> {

    public Class<T> entityType;

    public GenericDao() {
        entityType = (Class) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    public Session getCurrentSession() {
        return HibernateUtil.getXmlConfigSessionFactory().getCurrentSession();
    }

    public boolean doesExist(long id) {
        Session session = getCurrentSession();
        session.beginTransaction();

        T t = (T) getCurrentSession().get(entityType, id);

        session.getTransaction().commit();
        return t != null;
    }

    public void saveOrUpdate(T entity) {
        Session session = getCurrentSession();
        session.beginTransaction();

        session.saveOrUpdate(entity);

        session.getTransaction().commit();
    }

    public T find(long id) {
        Session session = getCurrentSession();
        session.beginTransaction();

        T t = (T) session.get(entityType, id);

        session.getTransaction().commit();
        return t;
    }

    public List<T> findAll() {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(entityType);
        List<T> allItemList = criteria.addOrder(Order.asc("id")).list();

        session.getTransaction().commit();
        return allItemList;
    }

    public void update(T updatedEntity) {
        Session session = getCurrentSession();
        session.beginTransaction();

        session.update(updatedEntity);

        session.getTransaction().commit();
    }

    public void delete(long id) {
        Session session = getCurrentSession();
        session.beginTransaction();

        T t = (T) session.createCriteria(entityType)
                .add(Restrictions.eq("id", id))
                .uniqueResult();

        if (t != null) {
            session.delete(t);
        }

        session.getTransaction().commit();
    }

    public void delete(List<Long> ids) {
        Session session = getCurrentSession();
        session.beginTransaction();

        for (Long id : ids) {
            T t = (T) session.createCriteria(entityType)
                    .add(Restrictions.eq("id", id))
                    .uniqueResult();

            if (t != null) {
                session.delete(t);
            }
        }

        session.getTransaction().commit();
    }

    public void deleteAll() {
        Session session = getCurrentSession();
        session.beginTransaction();

        session.createQuery("delete from " + entityType.getSimpleName())
                .executeUpdate();

        session.getTransaction().commit();
    }

    public long countAll() {
        Session session = getCurrentSession();
        session.beginTransaction();

        Long count = (Long) session.createCriteria(entityType)
                .setProjection(Projections.rowCount())
                .uniqueResult();

        session.getTransaction().commit();
        return count;
    }
}
