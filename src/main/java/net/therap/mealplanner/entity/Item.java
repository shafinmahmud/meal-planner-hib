package net.therap.mealplanner.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author shafin
 * @since 11/13/16
 */
@Entity
@Table(name = "item")
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 11, nullable = false, unique = true)
    private long id;

    @Column(name = "name", length = 45, nullable = true, unique = true)
    private String name;

    public Item() {
    }

    public Item(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static String listToString(Collection<Item> list) {
        StringBuffer sb = new StringBuffer("[");
        Iterator<Item> iterator = list.iterator();

        while (iterator.hasNext()) {
            sb.append(iterator.next().getName());
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }

        return sb.append("]").toString();
    }
}
