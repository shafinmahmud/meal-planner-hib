package net.therap.mealplanner.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author shafin
 * @since 11/20/16
 */
@Entity
@Table(name = "login_info")
public class LoginInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 11, nullable = false, unique = true)
    private long id;

    @Column(name = "user_name", length = 45, nullable = true)
    private String userName;

    @Column(name = "password", length = 45, nullable = true)
    private String password;

    @OneToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public LoginInfo() {
    }

    public LoginInfo(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
