package net.therap.mealplanner.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@Entity
@Table(name = "meal")
public class Meal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 11, nullable = false, unique = true)
    private long id;

    @Column(name = "day", nullable = true)
    @Enumerated(EnumType.STRING)
    private Day day;

    @ManyToOne
    @JoinColumn(name = "meal_type_id")
    private MealType mealType;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "meal_item",
            joinColumns = {
                    @JoinColumn(name = "meal_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "item_id")
            }
    )
    private List<Item> itemList = new ArrayList<>();

    public Meal() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public MealType getMealType() {
        return mealType;
    }

    public void setMealType(MealType mealType) {
        this.mealType = mealType;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public static Day resolveDayType(String dayString) {
        dayString = dayString.toUpperCase().substring(0, 3);

        switch (dayString) {
            case "SAT":
                return Day.SAT;
            case "SUN":
                return Day.SUN;
            case "MON":
                return Day.MON;
            case "TUE":
                return Day.TUE;
            case "WED":
                return Day.WED;
            case "THU":
                return Day.THU;
            case "FRI":
                return Day.FRI;
            default:
                return Day.SUN;
        }
    }

    public enum Day {
        SAT, SUN, MON, TUE, WED, THU, FRI
    }
}
