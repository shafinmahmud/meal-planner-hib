package net.therap.mealplanner.entity;

import javax.persistence.*;
import java.util.*;

/**
 * @author shafin
 * @since 11/13/16
 */
@Entity
@Table(name = "meal_type")
public class MealType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 11, nullable = false, unique = true)
    private long id;

    @Column(name = "type", length = 45, nullable = true)
    private String type;

    @Column(name = "hour", length = 45, nullable = true)
    private Date hour;

    @OneToMany(mappedBy = "mealType", cascade = CascadeType.ALL)
    private List<Meal> mealList = new ArrayList<>();

    public MealType() {
    }

    public MealType(String type, Date hour) {
        this.type = type;
        this.hour = hour;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getHour() {
        return hour;
    }

    public void setHour(Date hour) {
        this.hour = hour;
    }

    public List<Meal> getMealList() {
        return mealList;
    }

    public void setMealList(List<Meal> mealList) {
        this.mealList = mealList;
    }
}
