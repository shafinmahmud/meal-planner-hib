package net.therap.mealplanner.dto;

import net.therap.mealplanner.controller.ConsoleView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/14/16
 */
public class WelcomeDto {

    private final String banner;
    private final String title;
    private final List<String> options;

    public WelcomeDto() {
        this.banner = ConsoleView.WELCOME_BANNER;
        this.title = "Surf your Daily Meals and Menus";
        this.options = new ArrayList<>();
        options.add("1. Available Item");
        options.add("2. Planned Menus");
        options.add("0. Exit");
    }

    public String getBanner() {
        return banner;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getOptions() {
        return options;
    }
}
