package net.therap.mealplanner.dto;

import java.util.List;

/**
 * @author shafin
 * @since 11/14/16
 */
public class TabularDto {

    private String banner;
    private String title;
    private long count;
    private List<String> options;

    private String tableHead;
    private List<String> tableRows;

    public TabularDto() {
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getTableHead() {
        return tableHead;
    }

    public void setTableHead(String tableHead) {
        this.tableHead = tableHead;
    }

    public List<String> getTableRows() {
        return tableRows;
    }

    public void setTableRows(List<String> tableRows) {
        this.tableRows = tableRows;
    }
}
