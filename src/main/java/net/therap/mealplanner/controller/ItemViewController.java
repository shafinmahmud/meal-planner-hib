package net.therap.mealplanner.controller;

import net.therap.mealplanner.service.ItemService;
import net.therap.mealplanner.util.RegexUtil;

import java.util.Scanner;

/**
 * @author shafin
 * @since 11/14/16
 */
public class ItemViewController {

    private static final String ITEM_DELETE_REGEX = "(?i)d\\[([0-9]+)\\]";
    private static final String ITEM_ADD_REGEX = "(?i)i\\[(.*)\\]";
    private static final String ITEM_UPDATE_REGEX = "(?i)u\\[([0-9]+)\\][\\s]*\\[(.*)\\]";

    private Scanner scanner;
    private ItemService itemService;

    public ItemViewController() {
        this.itemService = new ItemService();
        this.scanner = new Scanner(System.in);
    }

    public void view() {
        ConsoleView.viewTabularData(itemService.getItemsTable());

        String in = scanner.next();
        String sw = in;

        if (in.matches(ITEM_DELETE_REGEX)) {
            sw = "1";
        } else if (in.matches(ITEM_UPDATE_REGEX)) {
            sw = "2";
        } else if (in.matches(ITEM_ADD_REGEX)) {
            sw = "3";
        }

        switch (sw) {
            case "1":
                String removingId = RegexUtil.getAnyMatched(in, ITEM_DELETE_REGEX, 1);
                itemService.deleteItem(Integer.valueOf(removingId));

                ItemViewController itemView = new ItemViewController();
                itemView.view();
                break;
            case "2":
                String id = RegexUtil.getAnyMatched(in, ITEM_UPDATE_REGEX, 1);
                String newName = RegexUtil.getAnyMatched(in, ITEM_UPDATE_REGEX, 2);
                itemService.updateItem(Integer.valueOf(id), newName);

                itemView = new ItemViewController();
                itemView.view();
                break;
            case "3":
                String items = RegexUtil.getAnyMatched(in, ITEM_ADD_REGEX, 1);
                itemService.insertItems(items);

                itemView = new ItemViewController();
                itemView.view();
                break;
            case "4":
                WelcomeViewController homeView = new WelcomeViewController();
                homeView.view();
                break;
            case "0":
                ConsoleView.exit();
                break;
            default:
                itemView = new ItemViewController();
                itemView.view();
                System.out.println("Invalid Input.");
                break;
        }
    }
}
