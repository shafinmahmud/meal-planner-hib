package net.therap.mealplanner.controller;

import net.therap.mealplanner.dto.TabularDto;
import net.therap.mealplanner.dto.WelcomeDto;
import net.therap.mealplanner.util.HibernateUtil;

/**
 * @author shafin
 * @since 11/14/16
 */
public class ConsoleView {

    public static final String SEPARATION_LINE = "------------------------------------------------------------------";
    public static final String WELCOME_BANNER = "                    Welcome to MealPlanner!";
    public static final String AVAILABLE_ITEMS = "                         Available Item";
    public static final String PLANNED_MENUS = "                            Planned Menus";

    public static final String ITEMS_TITLE = "Currently Available Item: ";
    public static final String MENU_TITLE = "Currently Available Planned Menus: ";

    public static void viewHome(WelcomeDto dto) {
        System.out.println(SEPARATION_LINE);
        System.out.println(dto.getBanner());
        System.out.println(SEPARATION_LINE);
        System.out.println(dto.getTitle());

        if (dto.getOptions() != null) {
            for (String option : dto.getOptions()) {
                System.out.println(option);
            }
        }
    }

    public static void viewTabularData(TabularDto dto) {
        System.out.println(SEPARATION_LINE);
        System.out.println(dto.getBanner());
        System.out.println(SEPARATION_LINE);

        System.out.println(dto.getTableHead());

        if (dto.getTableRows() != null) {
            for (String row : dto.getTableRows()) {
                System.out.println(row);
            }
        }
        System.out.println(dto.getTitle() + dto.getCount());
        System.out.println("\n");

        if (dto.getOptions() != null) {
            for (String option : dto.getOptions()) {
                System.out.println(option);
            }
        }
    }

    public static void exit() {
        HibernateUtil.closeSessionFactory();
        System.exit(0);
    }
}
