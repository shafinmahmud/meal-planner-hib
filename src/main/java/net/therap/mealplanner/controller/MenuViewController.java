package net.therap.mealplanner.controller;

import net.therap.mealplanner.service.MealService;
import net.therap.mealplanner.util.RegexUtil;

import java.util.Scanner;

/**
 * @author shafin
 * @since 11/14/16
 */
public class MenuViewController {

    private static final String MENU_DELETE_REGEX = "(?i)d\\[([0-9]+)\\]";
    private static final String MENU_ADD_REGEX = "(?i)i\\[([a-z]+)-([a-z]+)-([0-9]+)-\\{(.*)\\}\\]";
    private static final String MENU_UPDATE_REGEX = "(?i)u\\[([0-9]+)\\][\\s]*\\[([a-z]+)-([a-z]+)-([0-9]+)-\\{(.*)\\}\\]";

    private Scanner scanner;
    private MealService mealService;

    public MenuViewController() {
        this.mealService = new MealService();
        this.scanner = new Scanner(System.in);
    }

    public void view() {
        ConsoleView.viewTabularData(mealService.getMenuTable());

        String in = scanner.next();
        String sw = in;

        if (in.matches(MENU_DELETE_REGEX)) {
            sw = "1";
        } else if (in.matches(MENU_UPDATE_REGEX)) {
            sw = "2";
        } else if (in.matches(MENU_ADD_REGEX)) {
            sw = "3";
        }

        switch (sw) {
            case "1":
                //D[id]
                String id = RegexUtil.getAnyMatched(in, MENU_DELETE_REGEX, 1);
                mealService.deleteMenu(Integer.valueOf(id));

                MenuViewController menuView = new MenuViewController();
                menuView.view();
                break;
            case "2":
                //U[id] [DAY-TYPE-HOUR-{item1,item2}]
                id = RegexUtil.getAnyMatched(in, MENU_UPDATE_REGEX, 1);
                String day = RegexUtil.getAnyMatched(in, MENU_UPDATE_REGEX, 2);
                String type = RegexUtil.getAnyMatched(in, MENU_UPDATE_REGEX, 3);
                String hour = RegexUtil.getAnyMatched(in, MENU_UPDATE_REGEX, 4);
                String items = RegexUtil.getAnyMatched(in, MENU_UPDATE_REGEX, 5);
                mealService.updateMeal(Integer.valueOf(id), day, type, Integer.valueOf(hour), items);

                menuView = new MenuViewController();
                menuView.view();
                break;
            case "3":
                //I[DAY-TYPE-HOUR-{item1,item2}]
                day = RegexUtil.getAnyMatched(in, MENU_ADD_REGEX, 1);
                type = RegexUtil.getAnyMatched(in, MENU_ADD_REGEX, 2);
                hour = RegexUtil.getAnyMatched(in, MENU_ADD_REGEX, 3);
                items = RegexUtil.getAnyMatched(in, MENU_ADD_REGEX, 4);
                mealService.insertMeal(day, type, Integer.valueOf(hour), items);

                menuView = new MenuViewController();
                menuView.view();
                break;
            case "4":
                WelcomeViewController homeView = new WelcomeViewController();
                homeView.view();
                break;
            case "0":
                ConsoleView.exit();
                break;
            default:
                System.out.println("Invalid Input.");
                menuView = new MenuViewController();
                menuView.view();
                break;
        }
    }
}
