package net.therap.mealplanner.controller;

/**
 * @author shafin
 * @since 11/13/16
 */
public class Main {

    public static void main(String[] args) {
        WelcomeViewController home = new WelcomeViewController();
        home.view();
    }
}
